#FROM artifactory.internal.t-mobile.com:8502/sre-pipeline/step/build/mvn:mvn-3.6.0-jdk-1.8.0_202-1 AS BUILD
FROM maven:3.3.9-jdk-8 AS BUILD
COPY . /tmp/
WORKDIR /tmp/
RUN mvn package

#Find internally owned JRE 8 image
FROM jolokia/alpine-jre-8
WORKDIR /app/
COPY --from=BUILD /tmp/target/parrot-1.0-SNAPSHOT.jar parrot.jar

ENTRYPOINT [ "java", "-jar", "/app/parrot.jar" ]
EXPOSE 8080

