package com.tmobile.devicefinance.model;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RepeatTest extends TestCase{
    /**
     * Create the test case
     *
     * @param testName testModel
     */
    public RepeatTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( RepeatTest.class );
    }

    public void testRepeat() {
        String expectedRepeatPharse = "test phrase";
        Repeat actualRepeat = new Repeat(expectedRepeatPharse);
 

        Assert.assertEquals(expectedRepeatPharse, actualRepeat.getIsayWhatYouSay());
        //Assert.assertEquals("Blah", actualRepeat);
       }

}