package com.tmobile.devicefinance.controller;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.tmobile.devicefinance.model.Repeat;

public class RepeatControllerTest extends TestCase{
    /**
     * Create the test case
     *
     * @param testName testController
     */
    public RepeatControllerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( RepeatControllerTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testRepeat() {
        String testPharse = "test phrase";
        RepeatController controller = new RepeatController();
        Repeat expectedRepeat = new Repeat(testPharse);
        Repeat actualRepeat = controller.repeat(testPharse);

        Assert.assertEquals(expectedRepeat.getIsayWhatYouSay(), actualRepeat.getIsayWhatYouSay());
       }

}