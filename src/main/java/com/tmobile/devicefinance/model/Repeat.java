package com.tmobile.devicefinance.model;

public class Repeat {
    private final String iSayWhatYouSay;

    public Repeat (String iSayWhatYouSay) {
        this.iSayWhatYouSay = iSayWhatYouSay;
    }

    public String getIsayWhatYouSay() {
        return iSayWhatYouSay;
    }
}