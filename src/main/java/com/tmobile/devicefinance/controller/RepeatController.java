package com.tmobile.devicefinance.controller;

import com.tmobile.devicefinance.model.Repeat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RepeatController {
    @RequestMapping(value = "/repeat", method = RequestMethod.GET, produces = "application/json")
    public Repeat repeat (@RequestParam(value="iSay", defaultValue="") String iSay) {
        return new Repeat(iSay);
    }
}