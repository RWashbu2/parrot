Feature: Parrot

Scenario Outline: I say something to the parrot
    Given I say <phrase> to the parrot
    When the parrot resonds
    Then the parrot should respond with <phrase>

Examples:
    | phrase       |
    | hello parrot |
    | what's up    |