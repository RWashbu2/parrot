package parrotcucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class Stepdefs {

    private Response response;
	private ValidatableResponse json;
    private RequestSpecification request;
    
    //private String ENDPOINT = "http://localhost:8080/repeat";

    private String ENDPOINT = System.getenv("ENDPOINT");


    @Given("I say (.*) to the parrot")
    public void i_say_something_to_the_parrot(String phrase) {
        request = given().param("iSay", phrase);
    }

    @When("the parrot resonds")
    public void the_parrot_responds() {
        response = request.when().get(ENDPOINT);
    }

    @Then("the parrot should respond with (.*)")
    public void the_parrot_should_respond_with(String expectedPharse) {
        response.then().body("isayWhatYouSay", equalTo(expectedPharse));
        //System.out.println(json);
    }


}
