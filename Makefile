docker-build:
	@docker build . -t parrot

docker-run:
	@docker-compose up --force-recreate

cucumber-test-report-local: 
	@cd cucumber/parrot-cucumber && \
		docker-compose -f docker-compose.yml up --abort-on-container-exit --force-recreate && \
		docker-compose -f docker-compose.yml down && \
		cp -R target/cucumber ../../.reports  && \
		rm -R target

cucumber-test-report: 
	@cd cucumber/parrot-cucumber && \
		docker-compose -f docker-compose.yml up --abort-on-container-exit --force-recreate && \
		docker-compose -f docker-compose.yml down 

unit-test-report:
	@docker run -w /app -v `pwd`:/app artifactory.internal.t-mobile.com:8502/sre-pipeline/step/build/mvn:mvn-3.6.0-jdk-1.8.0_202-1 mvn test && \
		cp -R target/site/jacoco .reports && \
		rm -R target