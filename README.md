# Parrot

## Description
This application exposes an API endpoint that will repeat back a query string parameter.

## Build
From the project directory run the following command.

`make docker-build`

## Run
From the project directory run the following command.

`make docker-run`

Sample request:

`curl http://localhost:8080/repeat?iSay=monkeybusiness`

Expected response:
`{"isayWhatYouSay":"monkeybusiness"}`

## Functional Test Results
[Cucumber Results](https://rwashbu2.gitlab.io/parrot/cucumber/)
